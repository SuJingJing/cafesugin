class CorrectionsController < ApplicationController
	def create
		@correction = current_user.corrections.build(correction_params)
		@correction.save

		redirect_to @correction.answer.question
	end

	private
    # Never trust parameters from the scary internet, only allow the white list through.
    def correction_params
      params.require(:correction).permit(:content, :user_id, :answer_id)
    end
end
